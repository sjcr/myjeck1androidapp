package com.example.fin.myjeck1app;

public interface Constants {
    String TAG = "RetrieveAccessToken";
    String TARGET_ID_URL = "http://192.168.102.45:9999/oauth/token";
    String SERVER_CLIENT_ID = "949870552432-p5o2v47qbr4tta17dde9bdqtoqvkcudg.apps.googleusercontent.com";
    String GRANT_TYPE = "password";
    String MYJACK_PREFERENCES = "MYJACK_PREFERENCES";
    String ACCESS_TOKEN = "";
    String REFRESH_TOKEN = "";
    String EXPIRES_IN = "";
    String TOKEN_TYPE = "";
}
