package com.example.fin.myjeck1app;

import android.content.Context;
import android.util.Base64;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class NetworkManager implements Constants {
    private static NetworkManager ourInstance;

    public RequestQueue requestQueue;

    public static synchronized NetworkManager getInstance(Context context) {
        if (null == ourInstance)
            ourInstance = new NetworkManager(context);
        return ourInstance;
    }

    private NetworkManager(Context context) {
        requestQueue = Volley.newRequestQueue(context.getApplicationContext());
    }

    public void sendGoogleToken(JSONObject IDJson, final CustomResponseListener<String> listener) {
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(
                Request.Method.POST, TARGET_ID_URL, IDJson,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.d(TAG + ": ", "somePostRequest Response : " + response.toString());
                            if (null != response.toString())
                                listener.getResult(response.getString("hello"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d(TAG, "Error: " + e.getMessage());
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                if (null != error.networkResponse) {
                    Log.d(TAG + ": ", "Error Response code: " + error.networkResponse.statusCode);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
        };
        requestQueue.add(jsonObjReq);
    }

    public void postNewComment(final String userAccount, final String password, final CustomResponseListener<String> listener) {

        StringRequest sr = new StringRequest(Request.Method.POST, TARGET_ID_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (null != response.toString()) {
                    listener.getResult(response);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (null != error.networkResponse) {
                    Log.d(TAG + ": ", "Error Response code: " + error.toString());
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", userAccount);
                params.put("password", password);
                params.put("grant_type", GRANT_TYPE);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return createBasicAuthHeader("Jack", "JackLovesMarry");
            }

            Map<String, String> createBasicAuthHeader(String username, String password) {
                Map<String, String> headerMap = new HashMap<String, String>();
                String credentials = username + ":" + password;
                String encodedCredentials = Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                headerMap.put("Authorization", "Basic " + encodedCredentials);
                headerMap.put("Content-Type", "application/x-www-form-urlencoded");
                Log.d("retr creds to server", headerMap.toString());
                return headerMap;
            }

        };
        requestQueue.add(sr);
    }
}
