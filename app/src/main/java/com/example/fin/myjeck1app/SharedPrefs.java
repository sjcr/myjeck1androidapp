package com.example.fin.myjeck1app;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPrefs implements Constants {
    public static void storeString(Context context, String fieldKey, String fieldValue) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(MYJACK_PREFERENCES, context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(fieldKey, fieldValue);
        editor.apply();
    }

    public static String retrieveString(Context context, String fieldKey) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(MYJACK_PREFERENCES, context.MODE_PRIVATE);
        return sharedPreferences.getString(fieldKey, "");
    }
}
