package com.example.fin.myjeck1app;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class SplashActivity extends Activity implements Constants {

    boolean mSignedIn = false;
    Date expiredIn = new Date();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
        Handler mHandler = new Handler();
        mHandler.postDelayed(new Runnable() {
            public void run() {
                new PrefetchData().execute();
            }
        }, 1000);

    }

    private class PrefetchData extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            if (!SharedPrefs.retrieveString(getApplicationContext(), ACCESS_TOKEN).equals("") &&
                    !SharedPrefs.retrieveString(getApplicationContext(), REFRESH_TOKEN).equals("")) {
                mSignedIn = true;
            } else {
                mSignedIn = false;
            }

            if (!SharedPrefs.retrieveString(getApplicationContext(), EXPIRES_IN).equals("")) {
                SimpleDateFormat sdf = new SimpleDateFormat("mm/dd/yyyy");
                try {
                    Date expiredIn = sdf.parse(SharedPrefs.retrieveString(getApplicationContext(), EXPIRES_IN));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if (new Date().after(expiredIn)) {
                    //TODO Send refresh token
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // After completing http call
            // will close this activity and launch main activity

            if (mSignedIn == true) {
                Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
                startActivity(intent);
            } else {
                Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                startActivity(intent);
            }
            // close this activity
            finish();
        }

    }

}
