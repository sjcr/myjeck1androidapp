package com.example.fin.myjeck1app;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ExpandableListView;
import android.widget.SimpleExpandableListAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class LocationsActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    String[] locations = {"DummyLocation1", "DummyLocation2", "DummyLocation3"};

    String[] dummyLocation1 = {"block1", "street1", "city1"};
    String[] dummyLocation2 = {"block2", "street2", "city2"};
    String[] dummyLocation3 = {"block3", "street3", "city3"};

    ArrayList<Map<String, String>> locationsMap;
    ArrayList<Map<String, String>> childDataItem;
    ArrayList<ArrayList<Map<String, String>>> childData;
    Map<String, String> m;

    ExpandableListView lvMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.locations);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        locationsMap = new ArrayList<Map<String, String>>();
        for (String location : locations) {
            m = new HashMap<String, String>();
            m.put("locationName", location);
            locationsMap.add(m);
        }

        String groupFrom[] = new String[]{"locationName"};
        int groupTo[] = new int[]{android.R.id.text1};
        childData = new ArrayList<ArrayList<Map<String, String>>>();

        childDataItem = new ArrayList<Map<String, String>>();
        for (String location1 : dummyLocation1) {
            m = new HashMap<String, String>();
            m.put("locationAttribute", location1);
            childDataItem.add(m);
        }

        childData.add(childDataItem);

        childDataItem = new ArrayList<Map<String, String>>();
        for (String location1 : dummyLocation2) {
            m = new HashMap<String, String>();
            m.put("locationAttribute", location1);
            childDataItem.add(m);
        }

        childData.add(childDataItem);

        childDataItem = new ArrayList<Map<String, String>>();
        for (String location1 : dummyLocation3) {
            m = new HashMap<String, String>();
            m.put("locationAttribute", location1);
            childDataItem.add(m);
        }

        childData.add(childDataItem);

        String childFrom[] = new String[]{"locationAttribute"};

        int childTo[] = new int[]{android.R.id.text1};

        SimpleExpandableListAdapter adapter = new SimpleExpandableListAdapter(this, locationsMap,
                android.R.layout.simple_expandable_list_item_1,
                groupFrom,
                groupTo,
                childData,
                android.R.layout.simple_list_item_1,
                childFrom,
                childTo);

        lvMain = (ExpandableListView) findViewById(R.id.locations);
        lvMain.setAdapter(adapter);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.action_add_location:
                addNewLocation();
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_locations) {

        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void addNewLocation() {

        FragmentAddLocation fragment = new FragmentAddLocation();
        FragmentTransaction ftrans = getFragmentManager().beginTransaction();
        ftrans.add(R.id.frgmCont, fragment);
        ftrans.commit();

        getFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                if (getFragmentManager().getBackStackEntryCount() == 0) finish();
            }
        });
    }
}
