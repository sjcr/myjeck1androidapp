package com.example.fin.myjeck1app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener, Constants {

    private AutoCompleteTextView mEmail;
    private EditText mPassword;
    private Button mNext;
    private Button mRegister;
    private TextView mEnterCreds;
    private View focusView = null;
    private boolean cancel = true;
    private boolean success = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup_activity);

        mEmail = (AutoCompleteTextView) findViewById(R.id.email);
        mNext = (Button) findViewById(R.id.sign_up_next_button);
        mRegister = (Button) findViewById(R.id.sign_up_register_button);
        mPassword = (EditText) findViewById(R.id.sign_up_password);
        mEnterCreds = (TextView) findViewById(R.id.enterCreds);

        mRegister.setOnClickListener(this);
        mNext.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sign_up_next_button:
                if (isEmailValid(mEmail.getText().toString())) {
                    mEmail.setVisibility(View.INVISIBLE);
                    mNext.setVisibility(View.INVISIBLE);
                    mPassword.setVisibility(View.VISIBLE);
                    mRegister.setVisibility(View.VISIBLE);
                    mEnterCreds.setText(getString(R.string.sign_up_text_password));
                } else {
                    mEmail.setError(getString(R.string.error_invalid_email));
                    focusView = mEmail;
                    cancel = true;
                    if (cancel) {
                        focusView.requestFocus();
                    }
                }
                break;
            case R.id.sign_up_register_button:
                if (isPasswordValid(mPassword.getText().toString())) {
                    JSONObject json = new JSONObject();
                    try {
                        json.put("username", mEmail.getText().toString());
                        json.put("password", mPassword.getText().toString());
                        json.put("grant_type", GRANT_TYPE);
                    } catch (JSONException e) {
                        Log.d(TAG, e.toString());
                    }
                    NetworkManager.getInstance(getApplicationContext()).postNewComment(mEmail.getText().toString(), mPassword.getText().toString(), new CustomResponseListener<String>() {
                        @Override
                        public void getResult(String result) {
                            if (result.toString() != null) {
                                Log.d(TAG, result.toString());
                                    /*SharedPrefs.storeString(getApplicationContext(), ACCESS_TOKEN, result.getString("access_token"));
                                    SharedPrefs.storeString(getApplicationContext(), REFRESH_TOKEN, result.getString("refresh_token"));
                                    SharedPrefs.storeString(getApplicationContext(), EXPIRES_IN, result.getString("expires_in"));*/

                            } else {
                                Toast.makeText(SignUpActivity.this, "Email or Password is incorrect", Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                } else {
                    mPassword.setError(getString(R.string.error_invalid_password));
                    focusView = mPassword;
                    cancel = true;
                    if (cancel) {
                        focusView.requestFocus();
                    }
                }
        }
    }

    private void goToHomeActivity() {
        if (success)
            startActivity(new Intent(this, HomeActivity.class));
    }

    private boolean isEmailValid(String email) {
        if (!email.contains("@") ||
                TextUtils.isEmpty(email) ||
                email.length() < 5 ||
                email.matches(".*@.*\\..*"))
            return false;
        else return true;
    }

    private boolean isPasswordValid(String password) {
        if (TextUtils.isEmpty(password) ||
                password.length() < 5 ||
                !password.matches(".*[0-9].*") ||
                !password.matches(".*[A-Z].*") ||
                !password.matches(".*[a-z].*")) return false;
        else return true;
    }
}
