package com.example.fin.myjeck1app;

public interface CustomResponseListener<T> {
    void getResult(T object);
}
